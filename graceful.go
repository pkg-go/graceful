package graceful

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"golang.org/x/sync/errgroup"
)

type Graceful struct {
	runners []Runner
}

func New() *Graceful {
	return new(Graceful)
}

// Register add a runner to the graceful
//
//  g := graceful.New()
//  g.Register(graceful.Runner(func(ctx context.Context) error {
// 		for {
// 			select {
// 			case <-ctx.Done():
// 				return ctx.Err()
// 			case <-time.After(2 * time.Second):
// 				fmt.Println("running..")
// 			}
// 		}
// 	}))
// 	if err := g.Run(context.Background()); err != nil {
// 		log.Fatalf("error running: %v\n", err)
// 	}
func (g *Graceful) Register(r Runner) {
	g.runners = append(g.runners, r)
}

// Run runs all runners in the graceful and wait for the signal to stop
//
//  g := graceful.New()
//  ...
//  if err := g.Run(context.Background()); err != nil {
//    log.Fatalf("error running: %v\n", err)
//  }
func (g *Graceful) Run(ctx context.Context) error {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	go func() {
		<-c
		cancel()
	}()

	gr, ctx := errgroup.WithContext(ctx)
	for _, runner := range g.runners {
		runner := runner
		gr.Go(func() error {
			return runner.Run(ctx)
		})
	}

	return gr.Wait()
}
