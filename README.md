# graceful

graceful shutdown pattern implementation

## Usage

```go
package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/pkg-go/graceful"
)

func main() {
	mux := http.NewServeMux()
	mux.Handle("/", http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Hello World!")
		},
	))

	g := graceful.New()

	g.Register(graceful.NewHTTPHandlerRunner(":8080", mux, 30*time.Second))

	g.Register(graceful.NewIntervalRunner(2*time.Second, func(ctx context.Context) error {
		log.Println("tick")
		if rand.Float32() < 0.3 {
			return errors.New("random error")
		}
		return nil
	}))

	if err := g.Run(context.Background()); err != nil {
		log.Fatalf("error running: %v\n", err)
	}
}
```

