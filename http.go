package graceful

import (
	"context"
	"net/http"
	"time"
)

type serverTask struct {
	*http.Server
}

func (s serverTask) Run(ctx context.Context) error { return s.ListenAndServe() }

// NewHTTPServerRunner creates a runner for a http server
//
//  mux := http.NewServeMux()
//  ...
//  server := &http.Server{
//    Addr:    ":8080",
//    Handler: mux,
//  }
//  g := graceful.New()
//  g.Register(graceful.NewHTTPServerRunner(server, 30*time.Second))
//  if err := g.Run(context.Background()); err != nil {
// 	  log.Printf("error running:+%v\n", err)
//  }
func NewHTTPServerRunner(server *http.Server, shutdownTimeout time.Duration) Runner {
	task := serverTask{server}
	return NewTaskRunner(task, shutdownTimeout)
}

// NewTaskRunner creates a runner for a http handler
//
//  mux := http.NewServeMux()
//  ...
//  g := graceful.New()
//  g.Register(graceful.NewHTTPHandlerRunner(":8080", mux, 30*time.Second))
//  if err := g.Run(context.Background()); err != nil {
//    log.Printf("error running:+%v\n", err)
//  }
func NewHTTPHandlerRunner(addr string, handler http.Handler, shutdownTimeout time.Duration) Runner {
	return RunnerFunc(func(ctx context.Context) error {
		server := &http.Server{
			Addr:    addr,
			Handler: handler,
		}
		serverRunner := NewHTTPServerRunner(server, shutdownTimeout)
		return serverRunner.Run(ctx)
	})
}
