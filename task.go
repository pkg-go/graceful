package graceful

import (
	"context"
	"time"
)

type Task interface {
	Run(ctx context.Context) error
	Shutdown(ctx context.Context) error
}

func NewTaskRunner(task Task, shutdownTimeout time.Duration) Runner {
	return RunnerFunc(func(ctx context.Context) error {
		done := make(chan error)
		go func() {
			done <- task.Run(ctx)
		}()

		select {
		case <-ctx.Done():
			sdCtx, cancel := context.WithTimeout(context.TODO(), shutdownTimeout)
			defer cancel()
			return task.Shutdown(sdCtx)
		case err := <-done:
			return err
		}
	})
}
