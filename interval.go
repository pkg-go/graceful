package graceful

import (
	"context"
	"time"
)

// NewIntervalRunner creates a runner for a task that runs periodically
//
//  g := graceful.New()
//  g.Register(graceful.NewIntervalRunner(2*time.Second, func(ctx context.Context) error {
//    fmt.Println("running every 2 seconds...")
//    return nil
//  }))
//  if err := g.Run(context.Background()); err != nil {
//    log.Fatalf("error running: %v\n", err)
//  }
func NewIntervalRunner(interval time.Duration, f func(context.Context) error) Runner {
	return RunnerFunc(func(ctx context.Context) error {
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-time.After(interval):
				if err := f(ctx); err != nil {
					return err
				}
			}
		}
	})
}
